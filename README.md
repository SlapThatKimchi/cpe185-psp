# README #

This README would normally document whatever steps are necessary to get your application up and running.

# PSP (Parking Sensor Project) #
* Group members: Alexander Han, Nick Bergamini, Cooper Gooch, Hughson Garnier

# Overview #
* Project Descriptions:
* This project is about parking sensor and public server management. In real life, private parking places can implement these systems in order to keep track of an available parking spot for customers. Moreover, it should record the time spent for current customers as well, possibly being used as a parking meter.
* Our project is simple version of the real-life application. We are going to have multiple ping sensors (about 4) for vehicle detection and one propeller as a core hardware. Externally, we will have a raspberry pi for server networking.
* Nick will work on raspberry pi for server networking. So, propeller and server can communicate each other.
* Hughson will work on wireless Bluetooth connection between the ping sensor setups and a propeller.
* Cooper will work on the software GUI to make user accessible and friendly website.
* Alex will work on propeller coding for parking sensor operation.





# Positions #
* Nick - Raspberry Pi to Web Server Connection
* Alex - Propellor w/ Ping sensor to Detect Cars
* Cooper - Website GUI
* Sony - Bluetooth connection from Propellor to Raspberry Pi


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact